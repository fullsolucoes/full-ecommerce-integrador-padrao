﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IntegradorPadrao
{
    public class Integrador
    {
        Ferramentas f = new Ferramentas();
        private readonly IHttpClientFactory _clientFactory;
        private static AppSettings AppSettings { get; set; }
        private static string token { get; set; }
        public Integrador(IOptions<AppSettings> settings, IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            AppSettings = settings.Value;
        }

        public async Task<bool> GetToken()
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "autenticacao/autenticar");
                var client = _clientFactory.CreateClient("fullecommerce");

                List<KeyValuePair<string, string>> keyValues = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("erp_key", AppSettings.ErpKey),
                    new KeyValuePair<string, string>("store_key", AppSettings.StoreKey)
                };

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    token = (string)JObject.Parse(response.Content.ReadAsStringAsync().Result.Trim().Replace(" ", string.Empty))["token"];
                    return true;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Resposta de falha no webservice " + response.Content.ReadAsStringAsync().Result);
                    Console.ResetColor();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Falha grave em Gerar Token, consulte os logs no portal do ERP");
                Console.ResetColor();
                f.GravarLog(AppSettings.ErpName, "GetToken", ex.Message + " " + ex.InnerException);
                return false;
            }
        }

        public async Task<bool> Validartoken()
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, "autenticacao/validar");
                var client = _clientFactory.CreateClient("fullecommerce");

                if (string.IsNullOrEmpty(token))
                {
                    bool s = await GetToken();
                    if (s != true)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("Token estava nulo mas não foi possível obter um novo ");
                        Console.ResetColor();
                        return false;
                    }
                }

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erro ao validar o token");
                    Console.ResetColor();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Falha grave em Validar Token, consulte os logs no portal do ERP");
                Console.ResetColor();
                f.GravarLog(AppSettings.ErpName, "Validar Token", ex.Message + " " + ex.InnerException);
                return false;
            }
        }

        public async Task<bool> Atualizarprodutos()
        {
            Console.WriteLine("Atualização de produtos acionada " + DateTime.Now + "\n\n");
            Console.WriteLine("");
            Console.WriteLine("");
            bool s = await Validartoken();
            if (!s) { return false; }

            //checado que tenho os tokens válidos prossigo com o update
            string consulta = "";
            BDados db = new BDados(AppSettings);
            DataTable DT = new DataTable();

            try
            {
                db.AbrirConexao();
                if (AppSettings.viewConsulta.Replace("EC_", "").ToLower() != "")
                {
                    DT.Load(db.Comando("SELECT * FROM " + AppSettings.viewConsulta).ExecuteReader());
                }
                else
                {
                    DT.Load(db.Comando(AppSettings.sqlConsultaProdutos)).ExecuteReader();
                }
                db.FecharConexao();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Falha ao tentar ler a VIEW, consulte a pasta logs");
                Console.ResetColor();
                f.GravarLog(AppSettings.ErpName, "Leitura Produtos ERP", ex.InnerException + "\n\n" + ex.Message + "\n\n" + ex.Source + " " + ex.StackTrace + " " + ex.ToString());
                return false;
            }
            if(DT.Rows.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Atualização de produtos concluída mas nenhum produto foi selecionado");
                Console.ResetColor();
                return true;
            }

            int qntLote = 245;
            int lotes = (int)Math.Ceiling((double)DT.Rows.Count / qntLote);
            Console.WriteLine("");
            Console.WriteLine(DT.Rows.Count + " produtos encontrados");
            Console.WriteLine(lotes + " lotes serão enviados");
            Console.WriteLine("");
            for (int i = 0; i < lotes; i++)
            {
                DataTable dtLote = DT.Clone();
                dtLote.Clear();
                try
                {
                    for(int x = i*qntLote; x < (i+1)*qntLote; x++)
                    {
                        if (x < DT.Rows.Count)
                        {
                            dtLote.Rows.Add(DT.Select()[x].ItemArray);
                        }
                        else
                        {
                            break;
                        }
                    }
                    
                    consulta = JsonConvert.SerializeObject(dtLote);
                    consulta = consulta.ToLower().Replace("ec_", "");


                    if(dtLote.Rows.Count == 0)
                    {
                        break;
                    }
                    if (!Directory.Exists("_produtos"))
                    {
                        Directory.CreateDirectory("_produtos");
                    }
                    string fn = "_produtos/alteracao-lote" + i + "-" + Guid.NewGuid().ToString().Split("-")[0] + ".json";
                    using (StreamWriter writer = File.AppendText(fn))
                    {
                        writer.WriteLine(consulta);
                    }
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Falha ao tentar converter os Produtos em Json, consulte a pasta logs");
                    Console.ResetColor();
                    f.GravarLog(AppSettings.ErpName, "Parsear Json", ex.InnerException + "\n\n" + ex.Message + "\n\n" + ex.Source + " " + ex.StackTrace + " " + ex.ToString());
                    return false;
                }
                string retorno = null;

                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Post, "produtos/alterar");
                    var client = _clientFactory.CreateClient("fullecommerce");

                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    request.Content = new StringContent(consulta, Encoding.UTF8, "application/json");

                    var response = await client.SendAsync(request);
                    retorno = response.Content.ReadAsStringAsync().Result;

                    if (string.IsNullOrEmpty(retorno))
                    {
                        retorno = response.Content.ToString();
                    }
                    if (response.IsSuccessStatusCode)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine("Atualização de produtos concluída, total " + dtLote.Rows.Count + " produtos enviados");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("Erro, " + dtLote.Rows.Count + " produtos enviados mas a api reportou um erro, log com o json enviado foi salvo. \n\nRetorno " + retorno + "Status: " + response.StatusCode.ToString());
                        Console.ResetColor();
                        f.GravarLog(AppSettings.ErpName, "Erro no retorno da api", "Json " + consulta + "\n\nRetorno da API " + retorno);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Falha na conexão com o Webservice, consulte a pasta logs");
                    Console.ResetColor();
                    f.GravarLog(AppSettings.ErpName, "Envio do Json para a API", ex.InnerException + "\n\n" + ex.Message + "\n\n" + ex.Source + " " + ex.StackTrace + " " + ex.ToString());
                    return false;
                }
                JObject Jretorno = null;
                JArray Jincluir = null;

                try
                {
                    Jretorno = JObject.Parse(retorno);
                    Jincluir = JArray.FromObject(Jretorno.SelectToken("value").Where(x => x["status"].Value<string>() == "erro" && x["msg"].Value<string>() == "o item ainda não foi cadastrado"));
                    //Jincluir possui uma list com os skus a serem incluídos
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Falha ao obter o Json de retorno da alteração, consulte a pasta logs");
                    Console.ResetColor();
                    f.GravarLog(AppSettings.ErpName, "Parsear Json Retorno", "Retorno: " + retorno + ex.InnerException + "\n\n" + ex.Message + "\n\n" + ex.Source + " " + ex.StackTrace + " " + ex.ToString());
                    return false;
                }
                if (Jincluir.Count > 0)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine(Jincluir.Count + " produtos não estavam cadastrados ainda, efetuando a inclusão");
                    Console.ResetColor();

                    //consulta é um string com tudo que foi enviado antes, volto pra json pra filtrar
                    JArray Jprods = JArray.Parse(consulta);
                    List<string> skus = new List<string>();
                    foreach (JObject x in Jincluir)
                    {
                        skus.Add(x["sku"].ToString());
                    }
                    Console.WriteLine("skus que serão cadastrados: " + string.Join(",", skus));
                    consulta = JsonConvert.SerializeObject(Jprods.Where(x => skus.Contains((string)x["sku"])));
                    consulta = consulta.ToLower().Replace("ec_", "").Replace("_aprazo", "");

                    string fn = "_produtos/inclusao-" + Guid.NewGuid().ToString() + ".json";
                    using (StreamWriter writer = File.AppendText(fn))
                    {
                        writer.WriteLine(consulta);
                    }

                    var request = new HttpRequestMessage(HttpMethod.Post, "produtos/incluir");
                    var client = _clientFactory.CreateClient("fullecommerce");

                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    request.Content = new StringContent(consulta, Encoding.UTF8, "application/json");

                    var response = await client.SendAsync(request);
                    retorno = response.Content.ReadAsStringAsync().Result;

                    if (string.IsNullOrEmpty(retorno))
                    {
                        retorno = response.Content.ToString();
                    }
                    if (response.IsSuccessStatusCode)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine("Inclusão de produtos concluída, total " + Jincluir.Count + " produtos enviados");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("Erro, a api reportou um erro ao tentar incluir, log com o json enviado foi salvo. \n\nRetorno " + retorno + "Status: " + response.StatusCode.ToString());
                        Console.ResetColor();
                        f.GravarLog(AppSettings.ErpName, "Erro no retorno da api", "Json " + consulta + "\n\nRetorno da API " + retorno);
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(AppSettings.sqlAtualizaProdutos))
                {
                    try
                    {

                        JObject JretornoFinal = JObject.Parse(retorno);

                        int QntRegistros = 0;
                        foreach (JObject j in Jretorno["value"])
                        {
                            db.AbrirConexao();
                            QntRegistros += db.Comando(AppSettings.sqlAtualizaProdutos.ToLower().Replace("@ec_sku", j["sku"].ToString())).ExecuteNonQuery();
                            db.FecharConexao();
                        }
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine(QntRegistros + " registros atualizados no ERP");
                        Console.ResetColor();
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("Falha no Update do retorno, consulte a pasta logs");
                        Console.ResetColor();
                        f.GravarLog(AppSettings.ErpName, "Json: " + Jretorno["value"] + "Falha no Update do retorno", ex.InnerException + "\n\n" + ex.Message + "\n\n" + ex.Source + " " + ex.StackTrace + " " + ex.ToString());
                   
                    }
                }
            }
            return true;
        }

        public async Task<bool> BaixarCompras()
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Download de COMPRAS acionado " + DateTime.Now + "\n\n");
            Console.WriteLine("");
            bool s = await Validartoken();
            if (!s) { return false; }

            var request = new HttpRequestMessage(HttpMethod.Get, "vendas/listar");
            var client = _clientFactory.CreateClient("fullecommerce");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var response = await client.SendAsync(request);
            string retorno = response.Content.ReadAsStringAsync().Result;

            if (string.IsNullOrEmpty(retorno))
            {
                retorno = response.Content.ToString();
            }

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Sem compras para listar");
                Console.ResetColor();
                return true;
            }

            JObject Jretorno = JObject.Parse(retorno);

            if (response.IsSuccessStatusCode)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine("Consulta de vendas realizada com sucesso, " + Jretorno["value"].Count().ToString() + " importadas");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Erro ao tentar obter as compras");
                Console.ResetColor();
                return false;
            }

            if (!Directory.Exists("_compras"))
            {
                Directory.CreateDirectory("_compras");
            }

            List<ConfirmaCompra> cc = new List<ConfirmaCompra>();

            foreach (JObject c in Jretorno["value"])
            {
                string fn = "_compras/" + c["compra_id"] + ".json";
                if (!File.Exists(fn))
                {
                    using (StreamWriter writer = File.AppendText(fn))
                    {
                        writer.WriteLine(c);
                    }
                }
                cc.Add(new ConfirmaCompra
                {
                    compra_id = (long)c["compra_id"],
                    situacao = "ok"
                });
            }

            request = new HttpRequestMessage(HttpMethod.Post, "vendas/confirmar");
            client = _clientFactory.CreateClient("fullecommerce");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            request.Content = new StringContent(JsonConvert.SerializeObject(cc), Encoding.UTF8, "application/json");

            response = await client.SendAsync(request);

            if (Jretorno.Count == 100)
            {
                BaixarCompras();
            }

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }

    public class TimedHostedService : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly IOptions<AppSettings> settings;
        private readonly IHttpClientFactory clientFactory;
        private readonly ILogger<TimedHostedService> _logger;
        private static AppSettings AppSettings { get; set; }

        public TimedHostedService(ILogger<TimedHostedService> logger, IOptions<AppSettings> settings, IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
            _logger = logger;
            this.settings = settings;
            AppSettings = settings.Value;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            Console.ResetColor();
            Console.WriteLine("Serviço em segundo plano em execução.");
            _logger.LogInformation("Serviço em segundo plano em execução.");
            _timer = new Timer(TarefaTemporizada, null, TimeSpan.Zero, TimeSpan.FromMinutes(AppSettings.Timer));

            return Task.CompletedTask;
        }

        private async void TarefaTemporizada(object state)
        {
            Integrador integrador = new Integrador(settings, clientFactory);
            await integrador.Atualizarprodutos();
            await integrador.BaixarCompras();

            Console.ResetColor();
            Console.WriteLine("Executando serviço em segundo plano... " + DateTime.Now.ToString("pt-BR"));
            _logger.LogInformation("Executando serviço em segundo plano... " + DateTime.Now.ToString("pt-BR"));
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            Console.ResetColor();
            Console.WriteLine("Serviço em segundo plano está sendo interrompido.");
            _logger.LogInformation("Serviço em segundo plano está sendo interrompido.");
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

    }

    public class ConfirmaCompra
    {
        public long compra_id;
        public string situacao;
    }

}
