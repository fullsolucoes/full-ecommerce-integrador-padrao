﻿using FirebirdSql.Data.FirebirdClient;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;

namespace IntegradorPadrao
{
    public class Ferramentas
    {
        public void GravarLog(string Origem, string TipoDeErro, string Log)
        {
            if (!Directory.Exists("_logs"))
            {
                Directory.CreateDirectory("_logs");
            }
            string fn = "_logs/log-" + Guid.NewGuid().ToString() + ".txt";
            using (StreamWriter writer = File.AppendText(fn))
            {
                writer.WriteLine("Data: " + System.DateTime.Now);
                writer.WriteLine("\nOrigem: " + Origem);
                writer.WriteLine("\nTipo de Erro: " + TipoDeErro);
                writer.WriteLine("\nErro: " + Log + "\r\n\n");
            }
        }
    }
    public class AppSettings
    {
        public string ErpName { get; set; }
        public string ErpKey { get; set; }
        public string StoreKey { get; set; }
        public string Sgbd { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        public string Database { get; set; }
        public string viewConsulta { get; set; }
        public string sqlConsultaProdutos { get; set; }
        public string sqlAtualizaProdutos { get; set; }
        public long Timer { get; set; }
    }

    public class Parametros
    {
        public string propriedade { get; set; }
        public string valor { get; set; }
    }

    public class BDados
    {
        public static MySqlConnection MConex;
        public static FbConnection FConex;
        public static SqlConnection SSConex;
        public static NpgsqlConnection PGConex;
        public static OracleConnection OConex;
        public static OleDbConnection AcsConex;
        public AppSettings AppSettings { get; set; }
        public string SGBD { get; set; }

        public BDados(AppSettings settings)
        {
            AppSettings = settings;
            string host = AppSettings.Host;
            string database = AppSettings.Database;
            string user = AppSettings.User;
            string pass = AppSettings.Pass;
            string port = AppSettings.Port;
            SGBD = AppSettings.Sgbd;
            switch (SGBD)
            {
                case "Mysql":
                    MConex = new MySqlConnection();
                    MConex.ConnectionString = "server=" + host + ";password=" + pass + ";User Id=" + user + ";" +
                                                "Persist Security Info=True;database=" + database + ";" +
                                                "Convert Zero Datetime=True;Allow User Variables=True;SslMode=none;";
                    break;
                case "Firebird":
                    FConex = new FbConnection();
                    FConex.ConnectionString = "User=" + user + ";Password=" + pass + ";Database=" + database + ";" +
                                            "DataSource=" + host + ";Port = " + port + ";" +
                                            "Dialect = 3; Charset = NONE; Role =; Connection lifetime = 15; Pooling = false;" +
                                            "Packet Size = 8192; ServerType = 0; ";
                    break;
                case "SqlServer":
                    SSConex = new SqlConnection();
                    SSConex.ConnectionString = "server=" + host + ";password=" + pass + ";User Id=" + user + ";" +
                                                "database=" + database + ";";
                    break;
                case "Access":
                    AcsConex = new OleDbConnection();
                    AcsConex.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + host + ";password=" + pass + ";User Id=" + user + ";";
                    break;
                case "PostGre":
                    PGConex = new NpgsqlConnection();
                    PGConex.ConnectionString = "server=" + host + ";password=" + pass + ";User Id=" + user + ";" +
                                                "database=" + database + ";";
                    break;
                case "Oracle":
                    OConex = new OracleConnection();
                    OConex.ConnectionString = "server=" + host + ";password=" + pass + ";User Id=" + user + ";" +
                                                "database=" + database + ";";
                    break;
            }
        }

        public dynamic Conexao()
        {
            switch (SGBD)
            {
                case "Mysql":
                case "Local":
                    return MConex;
                case "Firebird":
                    return FConex;
                case "SqlServer":
                    return SSConex;
                case "PostGre":
                    return PGConex;
                case "Oracle":
                    return OConex;
                case "Access":
                    return AcsConex;
                default:
                    return null;
            }
        }
        public void AbrirConexao()
        {
            switch (SGBD)
            {
                default:
                case "Mysql":
                case "Local":
                    if (MConex.State != ConnectionState.Open)
                    {
                        MConex.Open();
                    }
                    break;
                case "Firebird":
                    if (FConex.State != ConnectionState.Open)
                    {
                        FConex.Open();
                    }
                    break;
                case "SqlServer":
                    if (SSConex.State != ConnectionState.Open)
                    {
                        SSConex.Open();
                    }
                    break;
                case "PostGre":
                    if (PGConex.State != ConnectionState.Open)
                    {
                        PGConex.Open();
                    }
                    break;
                case "Oracle":
                    if (OConex.State != ConnectionState.Open)
                    {
                        OConex.Open();
                    }
                    break;
                case "Access":
                    if (AcsConex.State != ConnectionState.Open)
                    {
                        AcsConex.Open();
                    }
                    break;
            }
        }
        public void FecharConexao()
        {
            switch (SGBD)
            {
                default:
                case "Mysql":
                case "Local":
                    if (MConex.State == ConnectionState.Open)
                    {
                        MConex.Close();
                    }
                    break;
                case "Firebird":
                    if (FConex.State == ConnectionState.Open)
                    {
                        FConex.Close();
                    }
                    break;
                case "SqlServer":
                    if (SSConex.State == ConnectionState.Open)
                    {
                        SSConex.Close();
                    }
                    break;
                case "PostGre":
                    if (PGConex.State == ConnectionState.Open)
                    {
                        PGConex.Close();
                    }
                    break;
                case "Oracle":
                    if (OConex.State == ConnectionState.Open)
                    {
                        OConex.Close();
                    }
                    break;
                case "Access":
                    if (AcsConex.State == ConnectionState.Open)
                    {
                        AcsConex.Close();
                    }
                    break;
            }
        }
        public dynamic Comando(string sql)
        {
            switch (SGBD)
            {
                default:
                case "Mysql":
                case "Local":
                    MySqlCommand MC = new MySqlCommand(sql, this.Conexao());
                    return MC;
                case "Firebird":
                    FbCommand FC = new FbCommand(sql, this.Conexao());
                    return FC;
                case "SqlServer":
                    SqlCommand SC = new SqlCommand(sql, this.Conexao());
                    return SC;
                case "PostGre":
                    NpgsqlCommand PGC = new NpgsqlCommand(sql, this.Conexao());
                    return PGC;
                case "Oracle":
                    OracleCommand OC = new OracleCommand(sql, this.Conexao());
                    return OC;
                case "Access":
                    OleDbCommand AC = new OleDbCommand(sql, this.Conexao());
                    return AC;
            }
        }
        public string BuscaSimples(string Oque, int id = 0)
        {
            this.AbrirConexao();
            DataTable DT = new DataTable();
            if (Oque != "")
            {
                DT.Load(this.Comando("SELECT * FROM " + Oque + ((id == 0) ? "" : " WHERE id = " + id)).ExecuteReader());
            }
            else
            {
                DT.Load(this.Comando(AppSettings.sqlConsultaProdutos)).ExecuteReader();
            }
            this.FecharConexao();
            if (DT.Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(DT);
            }
            else
            {
                return "{sucesso:false,motivo:\"sem dados para listar\"}";
            }

        }
        public string BuscaFk(string Oque, string Fk, string FkId)
        {
            this.AbrirConexao();
            DataTable DT = new DataTable();
            DT.Load(this.Comando("SELECT * FROM " + Oque + " WHERE " + Fk + " = " + FkId).ExecuteReader());
            this.FecharConexao();
            if (DT.Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(DT);
            }
            else
            {
                return "{sucesso:false,motivo:\"sem dados para listar\"}";
            }

        }
    }
}
